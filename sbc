#!/bin/bash

# sbc - a systemd-boot configurator
# Copyright (C) 2018  betseg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

declare -r myname="sbc"
declare -r myver="0.2.1"

checkroot() {
    if [ "$EUID" -ne 0 ]; then
        err "You need root permissions for this operation."
    fi
}

usage() {
    echo "${myname} ${myver}"
    echo "Usage: ${myname} [options]"
    echo
    echo "Options:"
    echo "  -v, --version   Print version"
    echo "  -b, --boot=dir  Set EFI boot partition mount point"
    echo "                   if present, has to be the first argument"
    echo "  -h, --help      Show this help"
    echo "  -s, --show      Print present configs"
    echo "  -l, --list      List all files in the EFI boot partition"
    echo "  -r, --read  <entry_name> [option]   Read a file"
    echo "                                       optionally with an option"
    echo "  -e, --edit  <entry_name option>     Edit a file"
    echo "  -n              Add a new option line with the same name."
    echo "                   Can only be used with -e, as the 4th argument"
    exit $1
}

version() {
    echo "${myname} ${myver}"
    echo "Copyright (C) 2018  İrem Ünlü"
    echo "License GPLv3+: GNU GPL version 2 or later"
    echo "<http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>"
    echo
    echo "This is free software; you are free to change and redistribute it."
    echo "There is NO WARRANTY, to the extent permitted by law."
    exit 0
}

info() {
    echo -e "\e[36;1m::\e[39m $1\e[0m"
}

err() {
    echo "$1" >&2
    exit 1
}

get_option() {
    [[ $1 ]] || usage 1

    if [ $1 == "loader" ]; then
        file="$boot_part""/loader/loader.conf"
    else
        file="$boot_part""/loader/entries/$1.conf"
    fi
    opt_select=$2

    if [ -z $opt_select ]; then
        cat $file
    else
        awk '/'^${opt_select}'/ { $1=""; print; }' ${file}
    fi

    exit 0
}

edit() {
    [[ $1 && $2 ]] || usage 1

    checkroot

    if [ $1 == "loader" ] || [ $1 == "loader.conf" ]; then
        file="$boot_part""/loader/loader.conf"
    else
        file="$boot_part""/loader/entries/$1.conf"
    fi
    opt_select=$2

    if [ ! -d "$boot_part/loader" ]; then
        info "$boot_part/loader doesn't exist. Creating."
        mkdir "$boot_part/loader"
    fi
    if [ ! -d "$boot_part/loader/entries" ]; then
        info "$boot_part/loader/entries doesn't exist. Creating."
        mkdir "$boot_part/loader/entries"
    fi

    if [ -f "$file" ]; then
        opt_line=$(cat -n "$file" | grep \
         "[[:space:]][0-9]*[[:space:]]$opt_select" | awk '{print $1;}')
        line_c=$(wc -w <<< "$opt_line")

        if [ "$3" == "-n" ]; then
            info "Adding a new line for the \"$2\" option."
            read -p "$2 " -i -e opt_new
            echo "$2 ""$opt_new" >> "$file"
        elif [ $line_c -gt 1 ]; then
            for line in $opt_line; do
                echo -n "$line: "
                sed -n "${line}p" $file
            done

            read -p "Select the line to edit: " \
             -i "$(tr '\n' ' ' <<< $opt_line)" -e line
            info "Editing option \"$2\" on line $line."

            opt_full=$(head -$line $file | tail -1)
            read -p "$2 " -i "$(echo $opt_full | cut -d' ' -f2-)" -e opt_new

            cp $file $file.save
            sed -i -e "${line}s|$opt_select.*|$opt_select $opt_new|" $file
        elif [ $line_c == 0 ]; then
            info "Adding a new option."
            read -p "$2 " -i -e opt_new
            echo "$2 ""$opt_new" >> "$file"
        else
            info "Editing option \"$2\"."

            opt_full=$(head -$opt_line $file | tail -1)

            read -p "$2 " -i "$(echo $opt_full | cut -d' ' -f2-)" -e opt_new

            cp $file $file.save
            sed -i -e "s|$opt_select.*|$opt_select $opt_new|" $file
        fi
    else
        info "Creating file \"$file\"."
        read -p "$2 " -i -e opt_new
        echo "$2 ""$opt_new" >> "$file"
    fi
    exit 0
}

configs() {
    info "loader.conf:"
    cat "$boot_part""/loader/loader.conf"
    echo
    info "Entries:"
    for file in "$boot_part"/loader/entries/*; do
        echo -e "\e[1m "$(sed "s|$boot_part/loader/entries/||" <<< $file) "\e[0m"
        cat $file
    done
    exit 0
}

list() {
    ls "$boot_part" -AlR --color=auto --group-directories-first
    exit 0
}

#####

if [[ $1 == "--boot="* ]]; then
    boot_part="${1:7}"
    shift
elif [[ $1 == "-b" ]]; then
    boot_part="$2"
    shift 2
else
    boot_part="/boot"
fi

if [[ $1 ]]; then
    if [[ ${1:0:2} = -- ]]; then
        case "${1:2}" in
            help) usage 0 ;;
            version) version ;;
            show) configs ;;
            list) list ;;
            edit) edit $2 $3 $4 ;;
            read) get_option $2 $3 ;;
            *) err "'$1' is an invalid argument." ;;
        esac
    elif [[ ${1:0:1} = - ]]; then
        case ${1:1} in
            h) usage 0 ;;
            v) version ;;
            s) configs ;;
            l) list ;;
            e) edit $2 $3 $4 ;;
            r) get_option $2 $3 ;;
            *) err "'$1' is an invalid argument." ;;
       esac
    else
        usage 1
    fi
else
    usage 1
fi
